---
title: 'Yelp Ratings vs City Development: Regression Analysis'
author: "Royce Yang"
date: "5/17/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(AER)
library(stargazer)
```

## Import Datasets
```{r}
data = read.csv("../yelp_dataset/2mildata.csv", na.strings=c("", " ","NA"))
```

## OLS Regression

```{r, results='asis'}
data$city_total_reviews_scaled = data$city_total_reviews / 1000000000
city_keeps = c("development5yr", "city_rating", "state",  "city_average_price", "city_total_reviews_scaled", "city_credit_acceptance")
city_data = unique(data[city_keeps])
mod2 = lm(development5yr ~ city_rating + state + city_average_price + city_total_reviews_scaled + city_credit_acceptance + city_rating * city_total_reviews_scaled, city_data)
stargazer(mod2, title = "The Effect of Yelp Ratings on City Development, with Controls",
          type = 'text', header = FALSE, omit="state", single.row = TRUE)
```

## IV Regression


```{r, results='asis'}
city_keeps = c("avg_user_leniency", "development5yr", "city_rating", "state",  "city_average_price", "city_total_reviews_scaled", "city_credit_acceptance")
city_data = unique(data[city_keeps])
mod4 = ivreg(development5yr ~ city_rating + city_average_price + city_total_reviews_scaled + city_credit_acceptance | avg_user_leniency + city_average_price + city_total_reviews_scaled + city_credit_acceptance, data = city_data)
stargazer(mod4, title = "The Effect of Yelp Ratings on City Development, with IV and Controls",
          type = 'text', header = FALSE, omit="state", single.row = TRUE)
```

